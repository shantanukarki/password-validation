import React, { useState, useEffect } from "react";
const listErrors = [
  {
    name: "lowercase",
    isDone: false,
    text: "Please Enter a lower case alphabet [a-z]",
    type: /[a-z]/g,
  },
  {
    name: "uppercase",
    isDone: false,
    text: "Please Enter a uppercase case alphabet [A-Z]",
    type: /[A-Z]/g,
  },
  {
    name: "special",
    isDone: false,
    text: "Please Enter a Special Character",
    type: /[ `!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/,
  },
  {
    name: "number",
    isDone: false,
    text: "Please Enter a Number from 1-9",
    type: /[0-9]/g,
  },
  {
    name: "length",
    isDone: false,
    text: "Please Enter Password over 12 characters",
    len: 12,
  },
  {
    name: "space",
    isDone: false,
    text: "Should not contain whitespace",
    type: /\s/,
  },
];
function App() {
  const [nameArray, setNameArray] = useState([]);
  const [userName, setUsername] = useState("");
  const [passwordErrors,setPasswordErrors] = useState(listErrors)
  const [different, setDifferent] = useState(false);

  // converts the given wors to array with three consecutive strings in the word
  const intoArray = (word) => {
    var newWord = word.replace(/\s+/g, "");
    var wordArray = newWord.split("");
    var newWordArray = [];
    for (let i = 0; i < wordArray.length; i++) {
      if (wordArray[i + 1] && wordArray[i + 2]) {
        var twoLetter = wordArray[i] + wordArray[i + 1] + wordArray[i + 2];
        newWordArray.push(twoLetter);
      }
    }
    return newWordArray;
  };
  // checks if there are any common three cosecutive terms in the given words
  const validate = (userTerm, inputTerm) => {
    for (let i = 0; i < userTerm.length; i++) {
      for (let j = 0; j < inputTerm.length; j++) {
        if (userTerm[i] === inputTerm[j]) {
          return true;
        }
      }
    }
    return false;
  };
  const handleuserNameChange = (e) => {
    const { value } = e.target;
    setUsername(value);
    setNameArray(intoArray(userName));
  };
  const handleChange = (e) => {
    const inputArray = intoArray(e.target.value);
    let check = validate(nameArray, inputArray);
    setDifferent(check);
    const newErrorList = passwordErrors.map((error) => {
      if (error.name === "length") {
        e.target.value.length >= error.len
          ? (error.isDone = true)
          : (error.isDone = false);
      } else if (error.name === "space") {
        !e.target.value.match(error.type)
          ? (error.isDone = true)
          : (error.isDone = false);
      } else {
        e.target.value.match(error.type)
          ? (error.isDone = true)
          : (error.isDone = false);
      }
      return error;
    });
    setPasswordErrors(newErrorList);
  };
  useEffect(() => {}, []);
  return (
    <div className="App">
      <header className="App-header">
        <label>Username</label>
        <input
          type="text"
          name="Username"
          value={userName}
          placeholder="Username"
          onChange={handleuserNameChange}
        />
        <input type="text" name="password" onChange={handleChange} />
        <div className="tooltip">
          <div className={`tooltip-text`}>
            <ul>
              {passwordErrors.map((error, index) => {
                return (
                  <li className={error.isDone ? "tick" : "cross"} key={index}>
                    {error.text}
                  </li>
                );
              })}
              <li className={different ? "cross" : "tick"}>
                Password should not exceed more than 2 consecutive characters
                from username, firstname and lastname
              </li>
            </ul>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
